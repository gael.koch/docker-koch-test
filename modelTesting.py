from sklearn import linear_model
from sklearn.ensemble import GradientBoostingRegressor
from dataFormatter import DataFormatter
from modelTester import ModelTester
from reportMaker import ReportMaker as rm
import Configuration as conf
#https://towardsdatascience.com/a-beginners-guide-to-text-classification-with-scikit-learn-632357e16f3a
#https://datascience.stackexchange.com/questions/39034/how-to-train-ml-model-with-multiple-variables

dataFormatter = DataFormatter()
modelTester = ModelTester()

def main():
    """
    test linear and gradient boosting models of scikit-learn.
    Generate data with different configurations (scaling, log, ...). Outputs the result in a markdown report
    """
    rm.startReport(rm, "output/ModelTestingReport-ElectromagneticML-Koch")
    #testing standard
    xTrain, xTest, yTrain, yTest = dataFormatter.getStandardSets(conf.MODEL_TEST_SIZE)
    xTrainS, xTestS, yTrainS, yTestS = dataFormatter.getStandardScaledSets(conf.MODEL_TEST_SIZE)
    launchTesting(xTrain, xTest, yTrain, yTest, xTrainS, xTestS, yTrainS, yTestS, False, False)

    #logarithmic testing
    xTrain, xTest, yTrain, yTest = dataFormatter.getLogSets(conf.MODEL_TEST_SIZE, False)
    xTrainS, xTestS, yTrainS, yTestS = dataFormatter.getLogScaledSets(conf.MODEL_TEST_SIZE, False)
    launchTesting(xTrain, xTest, yTrain, yTest, xTrainS, xTestS, yTrainS, yTestS, True, False)

    #added data testing
    xTrain, xTest, yTrain, yTest = dataFormatter.getLogSets(conf.MODEL_TEST_SIZE, True)
    xTrainS, xTestS, yTrainS, yTestS = dataFormatter.getLogScaledSets(conf.MODEL_TEST_SIZE, True)
    launchTesting(xTrain, xTest, yTrain, yTest, xTrainS, xTestS, yTrainS, yTestS, True, True)

    rm.endReport(rm)

def launchTesting(xTrain, xTest, yTrain, yTest, xTrainS, xTestS, yTrainS, yTestS, isLog, isDataAdded):
    """
    launch the testing of each model.
    :param xTrain: the features to train the model
    :param xTest: the features to test the model
    :param yTrain: the result to train the model
    :param yTest: the result to test the model
    :param xTrainS: the features to train the model (scaled)
    :param xTestS: the features to test the model (scaled)
    :param yTrainS: the result to train the model (scaled)
    :param yTestS: the result to test the model (scaled)
    :param isLog: true if the data contains the log of the inductance
    :param isDataAdded: true if the data contains the surface to height ratio
    """
    # create models (linear and gradient boosting)
    lineReg = linear_model.LinearRegression()
    gradBoost = GradientBoostingRegressor()
    lineRegScaled = linear_model.LinearRegression()
    gradBoostScaled = GradientBoostingRegressor()

    #prepare model title
    name = ""
    idMultiplier = 0
    if(isLog):
        name = "logarithmic "
        idMultiplier=4
    if(isDataAdded):
        name+=" +surface "
        idMultiplier=8

    # test models
    modelTester.testModel(lineReg, xTrain, xTest, yTrain, yTest, name+"linear regression", idMultiplier+1, isLog)
    modelTester.testModel(gradBoost, xTrain, xTest, yTrain, yTest, name+"gradient boosting", idMultiplier+2, isLog)
    modelTester.testModel(lineRegScaled, xTrainS, xTestS, yTrainS, yTestS, name+"scaled linear regression", idMultiplier+3, isLog)
    modelTester.testModel(gradBoostScaled, xTrainS, xTestS, yTrainS, yTestS, name+"scaled gradient boosting", idMultiplier+4, isLog)


if __name__ == "__main__":
    """
    main program entry point
    """
    main()

