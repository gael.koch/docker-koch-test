from mdutils.mdutils import MdUtils

class ReportMaker:
    """
    helper to generate mardown files
    """
    mdFile = None
    savedData=[]
    savedRow=0
    savedCol=0

    def startReport(self, fileName):
        """
        start the markdown report. Open up the file and add a title
        :param fileName: the name of the report
        """
        self.mdFile = MdUtils(file_name=fileName, title="sklearn report model testing")
        self.mdFile.new_header(level=1, title='Introduction')
        self.mdFile.new_paragraph("Ce fichier contient tous les résultats du test des modèles "
                             "de machine learning de sklearn")
    def endReport(self):
        """
        save all the content of the report and close the file.
        Also add the resume table of the scores and a table of content
        """
        self.mdFile.new_table_of_contents(table_title='Contents', depth=5)
        self.addSection(self, "Resume")
        self.addTable(self, self.savedData, self.savedCol, self.savedRow)
        self.mdFile.create_md_file()

    def addSection(self, text):
        """
        add a title
        :param text: the title
        """
        self.mdFile.new_header(level=1, title=text)

    def addSubSection(self, text):
        """
        add a sub title
        :param text: the sub title
        """
        self.mdFile.new_header(level=2, title=text)

    def addParagraph(self, text):
        """
        add some text
        :param text: the text to add
        """
        self.mdFile.new_paragraph(text)

    def addImage(self, pathToImage, imageText):
        """
        add an image
        :param pathToImage: the url of the image
        :param imageText: the text to replace the image if it is not found
        """
        self.mdFile.new_line(self.mdFile.new_inline_image(text=imageText, path=pathToImage))

    def addTable(self, content, col, row):
        """
        add a table
        :param content: the content of the table
        :param col: the number of columns
        :param row: the number of rows
        """
        self.mdFile.new_table(columns=col, rows=row, text=content, text_align='center')

    def saveTableData(self, content, col, row):
        """
        save data to add in a resume table at the end of the report
        :param content: the content to save
        :param col: the number of columns it contains
        :param row: the number of rows it contains
        """
        self.savedRow+=row
        if(self.savedCol <= 0):
            self.savedCol+=col
        self.savedData.extend(content)