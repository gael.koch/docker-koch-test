import matplotlib.pyplot as plt
import sklearn.metrics as metrics
import numpy as np
from dataFormatter import DataFormatter
import pandas as pd
from reportMaker import ReportMaker as rm

class ModelTester:
    """
    train a scikit-learn machine learning model and test it.
    Contains a scoring algorithm and generates graphics.
    Saves all the result in a markdown report.
    """
    dataFormatter = DataFormatter()
    firstDataSaved=False

    def testModel(self, model, xTrain, xTest, yTrain, yTest, name, id, isLog):
        """
        main logic of the testing. Train the model and the generates scores and graphics
        :param model: the model to test
        :param xTrain: the features to train the model
        :param xTest: the features to test the model
        :param yTrain: the result to train the model
        :param yTest: the result to test the model
        :param name: the name of the model
        :param id: the id of the model
        :param isLog: true if the model contains the log of the inductance
        """
        rm.addSection(rm, name)

        model.fit(xTrain, yTrain)
        print("------------ " + name + " ------------")
        self.printScatter(model, xTest, yTest, isLog, name, id)
        self.printErrorsToValues(model, xTest, yTest, isLog, name, id)
        self.printScore(model, xTest, yTest, isLog, name)

    def printScatter(self, model, xTest, yTest, isLog, name, id):
        """
        generate a scatter graphic, one generic and one close to 0.
        Allows to see the predictions of the model according to the real values
        :param model: the model
        :param xTest: the features to test the model
        :param yTest: the result to test the model
        :param isLog: true if the model contains the log of the inductance
        :param name: the name of the model
        :param id: the id of the model
        """
        rm.addSubSection(rm, "Scatter des prédictions")

        if(isLog):
            x_H = self.dataFormatter.reverseLog(yTest)
            y_H = self.dataFormatter.reverseLog(model.predict(xTest))
        else:
            x_H=yTest
            y_H = model.predict(xTest)
        fig, ax = plt.subplots()
        ax.scatter(x_H, y_H, edgecolors=(0, 0, 0), color='g')
        ax.plot([x_H.min(), x_H.max()], [x_H.min(), x_H.max()], 'k--', lw=4)
        ax.set_xlabel('Prediction_H')
        ax.set_ylabel('Inductance_H')
        plt.title(name + " - vue d'ensemble des prédictions")

        #TODO : REMOVE
        plt.ylim([0, 6e-6])

        path = "output/images/"+str(id)+"-scatterBig.png"
        fig.savefig(path, bbox_inches='tight')
        rm.addImage(rm, "images/"+str(id)+"-scatterBig.png", "vue d'ensemble des prédictions par rapport aux vraies valeurs")

        df = pd.DataFrame()
        df['inductance'] = x_H
        df['prediction'] = y_H
        df = df.sort_values('inductance')

        df['inductance'] = df['inductance'][: int(0.05*df['inductance'].size)]
        df['prediction'] = df['prediction'][: int(0.05*df['prediction'].size)]
        fig, ax = plt.subplots()
        ax.scatter(df['inductance'], df['prediction'], edgecolors=(0, 0, 0), color='g')

        ax.plot([df['inductance'].min(), df['inductance'].max()], [df['inductance'].min(), df['inductance'].max()], 'k--', lw=4)
        ax.set_xlabel('Prediction_H')
        ax.set_ylabel('Inductance_H')
        plt.title(name + " - vue zoomer en 0 des prédictions")

        # TODO : REMOVE
        plt.ylim([0, 5.5e-8])

        path = "output/images/" + str(id) + "-scatterSmall.png"
        fig.savefig(path, bbox_inches='tight')
        rm.addImage(rm, "images/" + str(id) + "-scatterSmall.png", "Zoom sur les petites valeurs des prédictions par rapport aux vraies valeurs")

    def printErrorsToValues(self, model, xTest, yTest, isLog, name, id):
        """
        generate a 2 graphic, one with the absolute and one with the relative error
        according to the real values.
        Allows to see how bad / good the model is
        :param model: the model
        :param xTest: the features to test the model
        :param yTest: the result to test the model
        :param isLog: true if the model contains the log of the inductance
        :param name: the name of the model
        :param id: the id of the model
        """
        rm.addSubSection(rm, "Erreur des prédictions")

        if (isLog):
            x_H = self.dataFormatter.reverseLog(yTest)
            y_H = self.dataFormatter.reverseLog(model.predict(xTest))
        else:
            x_H = yTest
            y_H = model.predict(xTest)

        differenceAbsolute_H = np.absolute(x_H - y_H)
        relativeError_prct = np.abs((x_H - y_H) / x_H) * 100
        df = pd.DataFrame()
        df['inductance'] = x_H
        df['predictionAbsolute'] = differenceAbsolute_H
        df['predictionRelative'] = relativeError_prct
        df = df.sort_values('inductance')

        #code taken from https://matplotlib.org/2.2.5/gallery/api/two_scales.html
        fig, ax1 = plt.subplots()
        xAxis = range(df['inductance'].count())

        color = 'tab:red'
        ax1.set_xlabel('test number')
        ax1.set_ylabel('absolute error_H', color=color)
        ax1.plot(xAxis, df['predictionAbsolute'], color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        #TODO : REMOVE
        ax1.set_ylim(0, 3e-6)

        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:blue'
        ax2.set_ylabel('Inductance_H', color=color)  # we already handled the x-label with ax1
        ax2.plot(xAxis, df['inductance'], color=color)
        ax2.tick_params(axis='y', labelcolor=color)

        # rolling mean
        ax3 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:green'
        ax3.plot(xAxis, df['predictionAbsolute'].rolling(50, min_periods=1).mean(), color=color)
        ax3.tick_params(axis='y', labelcolor=color)
        ax3.set_ylim(ax1.get_ylim())
        ax3.yaxis.tick_left()
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.title(name + " - erreur absolue des prédictions")

        path = "output/images/" + str(id) + "-errorAbsolute.png"
        fig.savefig(path, bbox_inches='tight')
        rm.addImage(rm, "images/" + str(id) + "-errorAbsolute.png", "Erreur absolue des prédictions par rapport aux vrai valeurs. "
                              "Trié selon l'inductance du plus petit au plus grand")

        fig, ax1 = plt.subplots()
        xAxis = range(df['inductance'].count())

        color = 'tab:red'
        ax1.set_xlabel('test number')
        ax1.set_ylabel('absolute relative error_%', color=color)
        ax1.plot(xAxis, df['predictionRelative'], color=color)
        ax1.tick_params(axis='y', labelcolor=color)

        #TODO : REMOVE
        ax1.set_ylim(0, 250)

        ax2 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:blue'
        ax2.set_ylabel('Inductance_H', color=color, verticalalignment='bottom')  # we already handled the x-label with ax1
        ax2.plot(xAxis, df['inductance'], color=color)
        ax2.tick_params(axis='y', labelcolor=color)

        #rolling mean
        ax3 = ax1.twinx()  # instantiate a second axes that shares the same x-axis
        color = 'tab:green'
        ax3.plot(xAxis, df['predictionRelative'].rolling(50, min_periods=1).mean(), color=color)
        ax3.tick_params(axis='y', labelcolor=color)
        ax3.set_ylim(ax1.get_ylim())
        ax3.yaxis.tick_left()
        fig.tight_layout()  # otherwise the right y-label is slightly clipped
        plt.title(name + " - erreur absolue relative des prédictions")


        path = "output/images/" + str(id) + "-errorRelative.png"
        fig.savefig(path, bbox_inches='tight')
        rm.addImage(rm, "images/" + str(id) + "-errorRelative.png", "Erreur absolue relative des prédictions par rapport aux vrai valeurs. "
                              "Trié selon l'inductance du plus petit au plus grand")


    def printScore(self, model, xTest, yTest, isLog, name):
        """
        computes some score, like the mean relative error for exemple
        :param model: the model
        :param xTest: the features to test the model
        :param yTest: the result to test the model
        :param isLog: true if the model contains the log of the inductance
        :param name: the name of the model
        """
        rm.addSubSection(rm, "Scoring du modèle")

        #prepare the relativ error percentage
        prediction_H = model.predict(xTest)

        if (isLog):
            x_H = self.dataFormatter.reverseLog(prediction_H)
            y_H = self.dataFormatter.reverseLog(yTest)
        else:
            x_H = prediction_H
            y_H = yTest

        error_prct = np.abs((y_H - x_H) / y_H) * 100
        meanError_prct = np.mean(error_prct)
        differenceAbsolute_H = np.absolute(y_H - x_H)


        tableContent = ["Métrique", "Valeur"]
        tableContent.extend(["R^2", str(np.round(model.score(xTest, yTest), 3))]) # r^2  coefficient of determination
        tableContent.extend(["Min erreur absolue : ", f'{differenceAbsolute_H.min():0.3e}'])
        tableContent.extend(["Max erreur absolue : ", f'{differenceAbsolute_H.max():0.3e}'])
        tableContent.extend(["Moyenne erreur absolue : ", f'{metrics.mean_absolute_error(y_H, x_H):0.3e}'])

        if(error_prct.min()<0.001):
            tableContent.extend(["Min erreur absolue relative : ", str(np.format_float_scientific(error_prct.min(), precision=3)) + "%"])
        else:
            tableContent.extend(["Min erreur absolue relative : ", str(np.round(error_prct.min(), 3)) + "%"])
        tableContent.extend(["Max erreur absolue relative : ", str(np.round(error_prct.max(), 3)) + "%"])
        tableContent.extend(["Moyenne erreur absolue relative : ", str(np.round(meanError_prct, 3)) + "%"])
        print("R2 : ", str(np.round(model.score(xTest, yTest), 3)))
        print("Moyenne erreur absolue relative : ", str(np.round(meanError_prct, 3)) + "%")

        rm.addTable(rm, tableContent, 2, 8)

        #save data for the resume
        if(not(self.firstDataSaved)):
            tableContentResume = ['Nom modèle', 'R^2', 'Min erreur absolue', 'Max erreur absolue', 'Moyenne absolue',
                                       'Min erreur absolue relative', 'Max erreur absolue relative', 'Moyenne erreur absolue relative']
            self.firstDataSaved = True
            rm.saveTableData(rm, tableContentResume, 8, 1)

        if (error_prct.min() < 0.001):
            minErrorRelative = str(np.format_float_scientific(error_prct.min(), precision=3)) + "%"
        else:
            minErrorRelative = str(np.round(error_prct.min(), 3)) + "%"

        tableContentResume = [name,
                              str(np.round(model.score(xTest, yTest), 3)),
                              f'{differenceAbsolute_H.min():0.3e}',
                              f'{differenceAbsolute_H.max():0.3e}',
                              f'{metrics.mean_absolute_error(y_H, x_H):0.3e}',
                              minErrorRelative,
                              str(np.round(error_prct.max(), 3)) + "%",
                              str(np.round(meanError_prct, 3)) + "%"]
        rm.saveTableData(rm, tableContentResume, 8, 1)
